# Link: https://www.hackerrank.com/challenges/nested-list/problem

if __name__ == '__main__':
  
  scoreList = []
  students = []
  names = []
  
  for _ in range(int(input())):
    name = input()
    score = float(input())
    
    scoreList.append(score)
    students.append([name, score])
  
  scoreList = list(set(scoreList))  #filter out duplicacy
  scoreList.sort()
  
  secondLowest = scoreList[1]
  
  for name, score in students:
    if score == secondLowest:
      names.append(name)
  
  names.sort()
  
  for i in range(len(names)):
      print(names[i])