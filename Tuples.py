# the code was run on pypy3, gives error in python

if __name__ == '__main__':
    n = int(input())
    integer_list = map(int, input().split())
    
    # my code
    tup = tuple(integer_list) # the input was already in list so type casting was the only thing it needed
    hash_tup = hash(tup)
    
    print(hash_tup)
