#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main()
{
       unsigned long long int arr[5];
    int i;

    for(i=0; i<5; i++)
    {
        scanf("%lld",&arr[i]);
    }

    long long max=arr[0], min=arr[0], sum=0;

    for(i=1; i<5; i++)
    {
        if(max<arr[i] )
        {
            max = arr[i];
        }

        if(min>arr[i])
        {
            min = arr[i];
        }
    }

    for(i=0; i<5; i++)
    {
        sum = sum + arr[i];
    }

    printf("%lld %lld",sum-max,sum-min);

    printf("\n");    
    return 0;
}