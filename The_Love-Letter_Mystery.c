#include <stdio.h>
#include <string.h>
#include <math.h>
int main()
{
    int q;
    scanf("%d", &q);
    while (q--)
    {
        char s[10000];
        scanf("%s", &s);

        int len = strlen(s), ans = 0;
        for (int i = 0, j = len - 1; i < len / 2; i++, j--)
        {
            if (s[i] - s[j])
            {
                ans = ans + abs(s[i] - s[j]);
            }
        }
        printf("%d\n", ans);
    }
}