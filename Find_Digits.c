#include <stdio.h>
int main()
{
    int t;
    scanf("%d", &t);
    while (t--)
    {
        long long int n, temp, rem, divCounter = 0;
        scanf("%lld", &n);

        temp = n;
        while (temp != 0)
        {
            rem = temp % 10;
            if (rem != 0 && n % rem == 0)
                divCounter++;
            temp = temp / 10;
        }
        printf("%lld\n", divCounter);
    }
}