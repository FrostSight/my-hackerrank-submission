#include <bits/stdc++.h>
using namespace std;
int main()
{
    int n, m;
    scanf("%d%d", &n, &m);

    vector<int> arr(n);
    vector<int> brr(m);

    for (int i = 0; i < n; i++)
        scanf("%d", &arr[i]);

    for (int i = 0; i < m; i++)
        scanf("%d", &brr[i]);

    int maxArr = 0, maxBrr = 0;
    for (int i = 0; i < n; i++)
    {
        if (maxArr < arr[i])
            maxArr = arr[i];
    }

    for (int i = 0; i < m; i++)
    {
        if (maxBrr < brr[i])
            maxBrr = brr[i];
    }

    int counterr = 0;
    for (int k = maxArr; k <= maxBrr; k++)
    {
        bool found = true;
        for (int i = 0; i < n; i++)
        {
            if (k % arr[i] != 0)
                found = false;
        }
        for (int i = 0; i < m; i++)
        {
            if (brr[i] % k != 0)
                found = false;
        }
        if (found)
            counterr++;
    }
    printf("%d\n", counterr);
}
