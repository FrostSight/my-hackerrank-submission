import java.util.Scanner;

public class ElectronicShop
{
    public static void main(String args[])
    {
        Scanner input = new Scanner(System.in);

        int b, n, m, i, j, rslt = -1;

        b = input.nextInt();
        n = input.nextInt();
        m = input.nextInt();

        int keyboard[] = new int [n];
        int drives[] = new int [m];

        for (i=0; i<n; i++)
            keyboard[i] = input.nextInt();

        for (j=0; j<m; j++)
            drives[j] = input.nextInt();

        for (i = 0; i<n; i++)
        {
            for (j=0; j<m; j++)
            {
                if(keyboard[i] + drives[j] <= b)
                {
                    rslt = Math.max(rslt, keyboard[i] + drives[j]);
                }
            }
        }

        System.out.println(rslt);
    }
}
