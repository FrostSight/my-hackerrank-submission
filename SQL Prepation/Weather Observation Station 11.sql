-- Link: https://www.hackerrank.com/challenges/weather-observation-station-11/problem

-- approach 1:
SELECT DISTINCT City
FROM Station
WHERE (City NOT LIKE 'a%' 
       AND City NOT LIKE 'e%' 
       AND City NOT LIKE 'i%' 
       AND City NOT LIKE 'o%' 
       AND City NOT LIKE 'u%')
    OR
        (City NOT LIKE '%a' 
         AND City NOT LIKE '%e' 
         AND City NOT LIKE '%i' 
         AND City NOT LIKE '%o' 
         AND City NOT LIKE '%u');


-- approach 2:
SELECT DISTINCT City
FROM Station
WHERE City NOT LIKE "[aeiou]%"
    OR City NOT LIKE "%[aeiou]"; 
