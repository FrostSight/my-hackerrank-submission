-- Link: https://www.hackerrank.com/challenges/weather-observation-station-19/problem

--MySQL
SELECT ROUND(SQRT(
    POWER(MIN(Lat_n) - MAX(Lat_n), 2) +
    POWER(MIN(Long_w) - MAX(Long_w), 2)
    ), 4
)
FROM Station;
