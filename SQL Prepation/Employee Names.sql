-- Link: https://www.hackerrank.com/challenges/name-of-employees/problem

SELECT Name
FROM Employee
ORDER BY Name; -- ORDER BY Name ASC (ASC by default)
