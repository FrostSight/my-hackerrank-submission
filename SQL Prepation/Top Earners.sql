-- Link: https://www.hackerrank.com/challenges/earnings-of-employees/problem

-- approach 1:
-- works on MySQL, MSSQL
SELECT MAX(months * salary) AS max_earning, COUNT(*) AS employee_count
FROM Employee
WHERE (months * salary) = (SELECT MAX(months * salary) FROM Employee);

--approach 2:
-- works on MySQL
SELECT MAX(months * salary) INTO @max_earn
FROM Employee;

SELECT @max_earn, COUNT(employee_id) AS employee_count
FROM Employee
WHERE (months * salary) = @max_earn;;