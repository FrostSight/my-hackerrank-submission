-- Link: https://www.hackerrank.com/challenges/the-blunder/problem?

-- MySQL
WITH cte AS(
    SELECT Id, Name, CAST(REPLACE(Salary, '0', '') AS UNSIGNED) AS Salary -- cast means converting into neumeric after "Replace"
    FROM Employees
)

SELECT CEIL(AVG(Salary) - (SELECT AVG(Salary) FROM cte))
FROM Employees
;
