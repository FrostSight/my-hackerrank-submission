-- Link: https://www.hackerrank.com/challenges/asian-population/problem

SELECT SUM(City.Population)
FROM City
JOIN Country
ON CITY.CountryCode =  COUNTRY.Code
WHERE Country.Continent = "Asia";