-- Link: https://www.hackerrank.com/challenges/weather-observation-station-18/problem

-- MySQL
SELECT ROUND(
    ABS(MIN(Lat_n) - MAX(Lat_n)) + 
    ABS(MIN(Long_w) - MAX(Long_w)),
    4
)
FROM Station;