-- Link: https://www.hackerrank.com/challenges/more-than-75-marks/problem

--Worked on MySQL, Oracle
SELECT Name 
FROM STUDENTS
WHERE Marks > 75
ORDER BY SUBSTR(Name, -3), ID;

-- worked on MySQL, MSSQL
SELECT Name 
FROM Students
WHERE Marks > 75
ORDER BY RIGHT(Name, 3), Id ASC;
