-- Link: https://www.hackerrank.com/challenges/weather-observation-station-10/problem

-- approach 1:
SELECT DISTINCT CITY 
FROM STATION
WHERE CITY NOT LIKE '%a'
    AND CITY NOT LIKE '%e'
    AND CITY NOT LIKE '%i'
    AND CITY NOT LIKE '%o'
    AND CITY NOT LIKE '%u';


-- approach 2:
SELECT DISTINCT City
FROM Station
WHERE City NOT LIKE "%[aeiou]";

-- approach 3:
SELECT DISTINCT City
FROM Station
WHERE City LIKE "%[^aeiou]";