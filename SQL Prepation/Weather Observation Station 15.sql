-- Link: https://www.hackerrank.com/challenges/weather-observation-station-15/problem

-- MySQL
SELECT ROUND(Long_w, 4)
FROM Station
WHERE Lat_n = (
    SELECT MAX(Lat_n)
    FROM Station
    WHERE Lat_n < 137.2345
);

/*
Question explained:
first find the max Lat_n < 137.2345
than for this Lat_n find Long_w
*/

/*
Mistake:
SELECT Long_w ROUND(Long_w, 4) 
FROM Station 
WHERE MAX(Lat_n) < 137.2345;

Why: 
max can only be used with SELECT
max can not be used with '<'
so we needed a sub query.
*/