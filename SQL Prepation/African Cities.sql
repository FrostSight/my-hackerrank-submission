-- Link: https://www.hackerrank.com/challenges/african-cities/problem

-- worked in MySQL, MSSQL
SELECT City.Name
FROM City
JOIN Country
ON City.Countrycode = Country.Code
WHERE Country.Continent = "Africa";


SELECT Name
FROM City
WHERE Countrycode IN (
    SELECT Code
    FROM Country
    WHERE Continent = 'Africa'
);
