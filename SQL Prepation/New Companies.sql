-- Link: https://www.hackerrank.com/challenges/the-company/problem

--MySQL
SELECT c.company_code, c.founder, 
    COUNT(DISTINCT e.lead_manager_code) AS lead_managers, 
    COUNT(DISTINCT e.senior_manager_code) AS senior_managers, 
    COUNT(DISTINCT e.manager_code) AS managers, 
    COUNT(DISTINCT e.employee_code) AS employees
FROM Company AS c
LEFT JOIN Employee AS e
ON c.company_code = e.company_code
GROUP BY c.company_code, c.founder
ORDER BY c.company_code;
