-- Link: https://www.hackerrank.com/challenges/weather-observation-station-20/problem

-- MySQL
-- Approach 1:
WITH cte AS(
    SELECT Lat_n, 
        (SELECT COUNT(Lat_n) FROM Station WHERE Lat_n < S.Lat_n) AS count_less, 
        (SELECT COUNT(Lat_n) FROM Station WHERE Lat_n > S.Lat_n) AS count_greater
    FROM Station AS S 
)

SELECT ROUND(Lat_n, 4)
FROM cte
WHERE count_less = count_greater
;

-- Approach 2:
SELECT ROUND(S.Lat_n, 4)
FROM Station AS S
WHERE (
    SELECT COUNT(Lat_n)
    FROM Station
    WHERE Lat_n < S.Lat_n
    ) = (
        SELECT COUNT(Lat_n)
        FROM Station 
        WHERE Lat_n > S.Lat_n
    )
;
