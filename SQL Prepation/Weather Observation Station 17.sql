-- Link: https://www.hackerrank.com/challenges/weather-observation-station-17/problem

--MySQL
SELECT ROUND(Long_w, 4)
FROM Station
WHERE Lat_n = (
    SELECT MIN(Lat_n)
    FROM Station
    WHERE Lat_n > 38.7780
);
