-- Link: https://www.hackerrank.com/challenges/average-population-of-each-continent/problem

SELECT COUNTRY.Continent, AVG (CITY.Population)
FROM City
JOIN Country
ON CITY.CountryCode =  COUNTRY.Code
GROUP BY Country.Continent;



SELECT Continent, AVG(Population)
FROM (
    SELECT Country.Continent, City.Population
    FROM City, Country
    WHERE City.CountryCode = Country.Code
) AS Subquery
GROUP BY Continent;
