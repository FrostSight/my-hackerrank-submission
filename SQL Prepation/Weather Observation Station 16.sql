-- Link: https://www.hackerrank.com/challenges/weather-observation-station-16/problem

--MySQL
SELECT ROUND(Lat_n, 4) 
FROM Station 
WHERE Lat_n = ( 
    SELECT MIN(Lat_n) 
    FROM Station 
    WHERE Lat_n > 38.7780 
);

