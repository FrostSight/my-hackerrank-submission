SELECT
CASE
    WHEN (a + b > c) AND (c + b > a) AND (c + a > c)
    THEN CASE
            WHEN (a = b) AND (b = c) And (c = a) THEN "Equilateral"
            WHEN (a = b) OR (b = c) OR (c = a) THEN "Isosceles"
            ELSE "Scalene"
         END
    Else "Not A Triangle"
END
FROM Triangles;
