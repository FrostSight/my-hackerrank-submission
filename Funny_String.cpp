#include <bits/stdc++.h>
using namespace std;
int main()
{
    int q;
    scanf("%d", &q);
    while (q--)
    {
        string funny;
        cin >> funny;
        int j = funny.length() - 1;
        bool state = true;
        for (int i = 1; i < funny.length(); i++, j--)
        {
            if (abs(funny[i] - funny[i - 1]) != abs(funny[j] - funny[j - 1]))
            {
                state = false;
                break;
            }
        }
        if (state)
            printf("Funny\n");
        else
            printf("Not Funny\n");
    }
}
