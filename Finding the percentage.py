if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
    
    query_name = input()
    
    # my code
    
    mark = student_marks[query_name]  # accessing the of query_name (key) and mark is list
    avg_mark = sum(mark) / 3
    print(format(avg_mark, '.2f')) # print upto 2 decimal point
