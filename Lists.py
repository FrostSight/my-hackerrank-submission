# Link: https://www.hackerrank.com/challenges/python-lists/problem

if __name__ == '__main__':
    N = int(input())
    
    lis1 = []
    
    for i in range(N):
        cmd = input().split()
        
        if cmd[0] == "insert":
            lis1.insert(int(cmd[1]), int(cmd[2]))
        elif cmd[0] == "print":
            print(lis1)
        elif cmd[0] == "remove":
            lis1.remove(int(cmd[1]))
        elif cmd[0] == "append":
            lis1.append(int(cmd[1]))
        elif cmd[0] == "sort":
            lis1.sort()
        elif cmd[0] == "pop":
            lis1.pop()
        else:
            lis1.reverse()
            